#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Module urls principal.
    
    Sert essentiellement à rediriger vers :py:mod:`note.urls`.
    
    Rajoute aussi la config pour servir les fichiers statiques quand on
    est en mode DEBUG.
"""

from django.conf.urls import patterns, include, url

import settings

if settings.DEBUG:
    # On n'utilise pas ça en prod
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#: Erreur 404 custom
handler404 = "note.views.page_not_found"

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^' + settings.NOTE_ROOT_URL.strip('/'), include('note.urls')),
    url(r'', include('note.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    # On n'utilise pas ça en prod
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        (r'^%s/(?P<path>.*)$' % (settings.MEDIA_URL.strip('/')),
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT}),
    )
