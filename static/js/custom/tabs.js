var currentTab='#conso';
var was_conso_double = false;
$('#tab-labels li a').click(function(e){
    if (currentTab == '#transfert' && !was_conso_double) { //si on était sur transfert et qu'avant on était en conso-simple, on y retourne
	switch_conso_mode(false);
    }
    //Lors du clic sur un lien, on affiche l'onglet concerné
    $('#tab-labels li a, #tab-content .active').removeClass('active');
    $(this).addClass('active');
    currentTab = $(this).attr('href');
    $(currentTab).addClass('active');

    //Puis on s'occupe des mises en formes spéciales
    if (currentTab == '#transfert') {
        was_conso_double = double_stack_mode;
        switch_conso_mode(true);
        $('#conso-double-btn').addClass('hide');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
        $('.transfert_conso_double').removeClass('hide');
        if(double_stack_mode){
            $('.transfert_conso_double').addClass('hide');
            $('#conso-double-btn').addClass('hide');
            $('#stack_container_2, #search_2').removeClass('hide');
        }
    }
    else if (currentTab == '#credit') {
        $('#stack_container_2, #search_2, #conso-double-btn').addClass('hide');
        $('#stack_label').addClass('hide');
	if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    else if (currentTab == '#retrait') {
        $('#stack_container_2, #search_2, #conso-double-btn').addClass('hide');
        $('#stack_label').addClass('hide');
	if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    else
    {
        $('#stack_label_2').addClass('hide');
        $('#stack_label').addClass('hide');
        if(double_stack_mode){
            $('#stack_container_2, #conso-double-btn').removeClass('hide');
            $(' #search_2').addClass('hide');
        }
        else
        {
            $('#stack_container_2, #search_2, #conso-double-btn').addClass('hide');
        }
    }
    e.preventDefault();

});



$('#id_search_field').keypress(function(e) {
    if(e.which == 13) {
  var prout =$( "#liste_notes li span" ).first().trigger( "click");
    $('#id_search_field').val("");
    }
});

$('#id_search_field').click(function() {
    $('#id_search_field').val("");
    $('#liste_notes').empty();
});

$('#id_search_field_2').keypress(function(e) {
    if(e.which == 13) {
  var prout =$( "#liste_notes_2 li span" ).first().trigger( "click");
    $('#id_search_field_2').val("");
    }
});

$('#id_search_field_2').click(function() {
    $('#id_search_field_2').val("");
    $('#liste_notes_2').empty();
});


function switch_conso_mode(conso_double_wanted){
if (conso_double_wanted && !double_stack_mode) 
{
if (!(currentTab == '#retrait' || currentTab == '#credit')) {
        $('#stack_container_2, #conso-double-btn').removeClass('hide');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    if (currentTab == '#transfert') {
        $('#conso-double-btn').addClass('hide');
        $('.transfert_conso_double').addClass('hide');
        $('#search_2').removeClass('hide');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    }
    double_stack_mode =true;
    $('#conso-simple').css({"color": "#333333", "font-weight": "normal" });
    $('#conso-double').css({"color": "#000", "font-weight": "bold" });
}

if (!conso_double_wanted && double_stack_mode){
    if (!(currentTab == '#retrait' || currentTab == '#credit')) {
        $('#stack_container_2, #conso-double-btn').addClass('hide');
    }
    if (currentTab == '#transfert') {
        $('.transfert_conso_double').removeClass('hide');
        $('#search_2').addClass('hide');
    }
    double_stack_mode =false;
    $('#conso-double').css({"color": "#333333", "font-weight": "normal" });
    $('#conso-simple').css({"color": "#000", "font-weight": "bold" });
}


}


$('#conso-double').click(function(e){
    switch_conso_mode(true)
});



$('#conso-simple').click(function(e){
    switch_conso_mode(false)

});



