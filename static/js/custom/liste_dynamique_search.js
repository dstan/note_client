/* Script JS qui permet d'afficher une liste dynamique
   des notes en faisant un search */

/** Il intéragit avec 3 éléments de la page, repérés par leur id :
 - "id_search_field" : l'élément dans lequel on récupère le terme recherche
                       et aussi où sera placé le pseudo/alias/… quand on clique sur une note
 - "liste_comptes" : l'élément (tableau) qui sera remplacé par la liste des notes obtenue
**/

/* fonctions de hl et de cliquabilité des lignes du tableau de recherche */
var readhesion = false;
function ChangeColor(tableRow, highLight)
{
    if (highLight)
    {
        tableRow.style.backgroundColor = '#dcdcdc';
    }
    else
    {
        tableRow.style.backgroundColor = 'white';
    }
}

function GoTo(url)
{
    window.location = url;
}

/* fonction qui effectue la recherche puis appelle readData en callback */
function request(asked, callback) {
	var xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseText);
		}
	};
    /* On envoie la requête en POST */
    if (readhesion) {
    xhr.open("POST", NOTE_ROOT_URL + "search_readhesion/", true);
    }
    else {
    xhr.open("POST", NOTE_ROOT_URL + "search/", true);
    }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("asked=" + encodeURIComponent(asked));
}
/* fonction appelée à la fin du timer */
function getInfo() {
    var search_field = document.getElementById("id_search_field");
    var asked = search_field.value;
    /* on ne fait la requête que si on a au moins un caractère pour chercher */
    if (asked.length >= 1) {
        request(asked, readDataNormal);
    }
}

var timer;
var timer_on;
/* fontion appelée quand le texte change (délenche le timer) */
function search_field_moved(secondfield) {
    if (timer_on) //si le timer n'est pas en cours, on le lance et on enregistre le fait qu'il tourne.
    {
        clearTimeout(timer);
        timer = setTimeout("getInfo(" + secondfield + ")", 500);
    }
    else //On réinitialise le compteur.
    {
        timer = setTimeout("getInfo(" + secondfield + ")", 500);
        timer_on = true;
    }
}
/* wrapper pour readData */
function readDataNormal(oData) {
    return readData(oData);
}

/* fonction qui traite les données à leur retour */
function readData(oData) {
	var gotlist = JSON.parse(oData);
	var liste = document.createElement("table");
    liste.setAttribute("class", "table table-striped");
    // on crée la ligne de titre

    var thead = document.createElement("thead");
    var first_line = document.createElement("tr");
    thead.appendChild(first_line);
    var caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("idbde"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Nom"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Prénom"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Pseudo"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Aliases"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Anciens pseudos"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("E-mail"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Solde"));
    first_line.appendChild(caze_th)
    caze_th = document.createElement("th");
    caze_th.appendChild(document.createTextNode("Section"));
    first_line.appendChild(caze_th)
    var ligne, caze;
	var size = gotlist.length;
    if (size>0)
    {
        liste.appendChild(thead);
    }
    var tbody = document.createElement("tbody");
	for (var i = 0, c = size; i<c; i++) {
        // on crée une nouvelle ligne
		ligne = document.createElement("tr");
        // on la remplit avec les champs du compte
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["idbde"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["nom"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["prenom"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["pseudo"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["aliases"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["historiques"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["mail"]));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode("" + (gotlist[i]["solde"]/100) + " €"));
        ligne.appendChild(caze);
        caze = document.createElement("td");
        caze.appendChild(document.createTextNode(gotlist[i]["section"]));
        ligne.appendChild(caze);
        // on rend la ligne cliquable
        if (readhesion) {
        ligne.addEventListener("click", GoTo.bind("trapped", NOTE_ROOT_URL + "readhesions/" + gotlist[i]["idbde"] + "/"));
        }
        else
        {
        ligne.addEventListener("click", GoTo.bind("trapped", NOTE_ROOT_URL + "comptes/" + gotlist[i]["idbde"] + "/"));
        }
        // on ajoute la ligne au tableau
        tbody.appendChild(ligne);
	}
	liste.appendChild(tbody);
    old_liste = document.getElementById("liste_comptes")
    // on lui donne le même id qu'avant pour pouvoir recommencer
    liste.setAttribute("id", "liste_comptes")
    var caption_object = document.createElement("caption")
    if (size>0)
    {
        var caption_text = document.createTextNode("Résultats de la recherche");
        caption_object.appendChild(caption_text);
    }
    else
    {
        var caption_text = document.createTextNode("Aucun compte ne correspond à ta recherche");
        caption_object.setAttribute("class", "search_failed");
        caption_object.appendChild(caption_text);
    }
    liste.appendChild(caption_object);
    old_liste.parentNode.replaceChild(liste, old_liste);
}
