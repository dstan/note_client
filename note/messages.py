#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Génération d'erreurs/warnings/success messages.
   
   Ils sont enregistrés dans la session (:py:mod:`django.contrib.messages`)
   pour être affichés à la prochaine page chargée (juste après en général).
   
   """

# Permet de stocker des messages dans la session courante
from django.contrib import messages

def add_message(request, messagelevel, message):
    """Enregistre un message dans la session avec :py:meth:`django.contrib.messages.add_message`."""
    messages.add_message(request, messagelevel, message)

def add_error(request, message):
    """Enregistre une erreur"""
    add_message(request, messages.ERROR, message)

def add_warning(request, message):
    """Enregistre un warning"""
    add_message(request, messages.WARNING, message)

def add_success(request, message):
    """Enregistre un succès"""
    add_message(request, messages.SUCCESS, message)
