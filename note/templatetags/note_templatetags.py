#!/usr/bin/env python
# -*- encoding: utf-8 -*-

# Templates django custom pour la Note Kfet 2015

from django import template
import settings

from collections import namedtuple
import itertools
import time

register = template.Library()

@register.simple_tag
def euro(montant):
    """Affiche une somme en euros (à partir des centimes),
       avec le bon nombre de chiffres après la virgule"""
    try:
        return (u"%.2f\xa0€" % (montant/100.0)).replace(".", ",")
    except:
        return u''

@register.simple_tag
def render_datetime(timestamp):
    """Affiche une date en changeant son format (cf settings.py)"""
    try:
        timestamp = timestamp.rsplit(".", 1)[0] # pour se débarasser des éventuelles microsecondes
        date = time.strptime(timestamp, settings.DATETIME_INPUT_FORMAT)
        return time.strftime(settings.DATETIME_RENDERING_FORMAT, date)
    except:
        return ''

@register.filter
def groupby(items, num):
    """Pour lister par blocks de ``num`` éléments."""
    args = [iter(items)] * num
    return itertools.izip_longest(*args)

@register.filter
def buttons_groupby(items, double):
    """Filtre pour lister par blocks de 6 éléments quand on est en mode double stack, par 3 sinon."""
    if double:
        return groupby(items, 6)
    else:
        return groupby(items, 3)

@register.inclusion_tag('note/template_activite.html')
def render_activite(activite, isadmin=False, isgestion=False, ismine=False):
    """Affiche le tableau d'une activité"""
    Link = namedtuple('Link', ['label', 'target', 'style'])
    boutons = []
    
    if isadmin:
        if not isgestion:
            boutons.append(Link(
                    'Gérer',
                    '%sactivites/%s/gestion/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'info'))
    if isgestion:
        if activite["valideparpseudo"]:
            boutons.append(Link(
                    'Dévalider',
                    '%sactivites/%s/gestion/invalidate/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'danger'))
        else:
            boutons.append(Link(
                    'Valider',
                    '%sactivites/%s/gestion/validate/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'success'))
    
    if activite["liste"] and (isadmin or "caninvite" in activite and activite["caninvite"]):
        boutons.append(Link(
                'Inviter',
                '%sactivites/%s/%s' % (settings.NOTE_ROOT_URL, activite["id"], "" if not isadmin else "admin"),
                'warning' if (isadmin and activite["fails"]) else ''))
    
    if (ismine and activite["validepar"] == -100) or isgestion:
        boutons.append(Link(
                'Modifier',
                '%s%sactivites/%s/%s' % (settings.NOTE_ROOT_URL, "mes_" if ismine else "", activite["id"], "gestion/modifier/" if not ismine else ""),
                ''))
    
    if "candelete" in activite and activite["candelete"]:
        boutons.append(Link(
                'Supprimer',
                '%s%sactivites/%s/%sdelete/' % (settings.NOTE_ROOT_URL, "mes_" if ismine else "", activite["id"], "gestion/" if isgestion else ""),
                'danger'))
    
    return {
        'NOTE_ROOT_URL' : settings.NOTE_ROOT_URL,
        'activite': activite,
        'boutons': boutons,
        'isadmin': isadmin,
        'isgestion': isgestion,
        'ismine': ismine,
        }
