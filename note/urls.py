#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Associe chaque url à une fonction de :py:mod:`note.views` qui
    est chargée de générer la page correspondante.
"""

from django.conf.urls import patterns, include, url

#: Liste des patterns d'url
urlpatterns = patterns('note.views',
    # pages de base
    url(ur'^/*$', 'login_page'),
    url(ur'^/index/*$', 'index'),
    url(ur'^/logout/*$', 'logout'),
    # consos
    url(ur'^/consos(?P<double>-double)?/*$', 'consos'),
    url(ur'^/consos(?:-double)?/(?P<de>de)?valider_transaction/(?P<idtransaction>[^/]*)$', 'toggle_transaction'),
    # dons
    url(ur'^/(?:virements|dons)/*', 'dons'),
    # les activités et invitations
    url(ur'^/(?:activite|invitation)s?(?P<admin>/admin)?/*$', 'activites'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion(?P<validation>/validate|/invalidate|/delete)?/*$', 'activite_gestion'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion/modifier/*$', 'activite_gestion_modifier'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)(?P<admin>/admin)?/*$', 'activite'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/del_invite/(?P<idinv>[^/]*)(?P<admin>/admin)?/*$', 'del_invite'),
    # mes_activités = création d'activités
    url(ur'^/mes_activites(?:/(?P<idact>[^/]*))?(?P<delete>/delete)?/*$', 'mes_activites'),
    # la recherche et gestion des comptes
    url(ur'^/comptes_advanced/*$', 'comptes_advanced'),
    url(ur'^/search_historique_pseudo/*$', 'search_historique_pseudo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/*$', 'comptes'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique/*$', 'historique_transactions'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/*$', 'modifier_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/supprimer/*$', 'supprimer_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/password/*$', 'password'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/photo/*$', 'update_photo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/*$', 'aliases'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/delete(?P<delall>_all)?/(?P<idalias>[^/]*)/*$', 'unalias'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique_pseudo/*$', 'historique_pseudo'),
    #les réadhésions
    url(ur'^/readhesions/(?P<idbde>[^/]*)/*$', 'readhesions'),
    # la gestion des boutons
    url(ur'^/boutons/*$', 'boutons'),
    url(ur'^/boutons/(?P<idbouton>[^/]*)(?P<delete>/delete)?/*$', 'boutons'),
    # les préinscription
    url(ur'^/preinscriptions?/*$', 'preinscriptions'),
    # les inscriptions
    url(ur'^/inscriptions?(?:/(?P<preid>[^/]*))?(?P<delete>/delete)?/*$', 'inscriptions'),
    # regeneration du password
    url(ur'^/regen_pw/(?P<token>[^/]*)/*$', 'regen_pw'),
    # easter egg
    url(ur'^/(?:teapot|th(?:é|e)|coffee|caf(?:é|e))/*$', 'teapot'),
)

urlpatterns += patterns('note.ajaj',
    # les pages de requêtes AJAJ
    url(ur'^/quick_search/*$', 'quick_search'),
    url(ur'^/search/*$', 'search'),
    url(ur'^/search_readhesion/*$', 'search_readhesion'),
    url(ur'^/get_boutons/*$', 'get_boutons'),
    url(ur'^/get_display_info/*$', 'get_display_info'),
    url(ur'^/get_photo/(?P<idbde>[^/]*)/*$', 'get_photo'),
    url(ur'^/do_conso/*$', 'do_conso'),
    url(ur'^/do_(?P<action>credit|retrait)/*$', 'do_credit_retrait'),
    url(ur'^/do_transfert/*$', 'do_transfert'),
)
