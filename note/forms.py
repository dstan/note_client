#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from django import forms
import settings

import re
import time
import datetime

class CreditRetraitWithoutIdbde(Exception):
    """Erreur levée en cas de tentative de crédit ou retrait sans idbde."""
    pass

class LoginForm(forms.Form):
    """Formulaire de login"""
    username = forms.CharField(label="Pseudo")
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(render_value=False))
    droits = forms.ChoiceField(choices=[(k, settings.ACL_MASKS[k][0]) for k in settings._acl_masks_keys])

class InviteForm(forms.Form):
    """Formulaire d'invitation"""
    nom = forms.CharField(label="Nom")
    prenom = forms.CharField(label="Prénom")

class FrenchFloatField(forms.FloatField):
    """Un champ FloatField, mais qui accepte aussi la virgule comme séparateur"""
    def to_python(self, raw_value):
        """Conversion de la valeur texte en objet python."""
        return super(FrenchFloatField, self).to_python(raw_value.replace(",", "."))

class BaseCompteRelatedForm(forms.Form):
    """Classe de base pour tous les formulaires traitant un compte (même une préinscription)"""
    type = forms.ChoiceField(label="Type de compte", choices=[("personne", "Personne"), ("club", "Club")])
    nom = forms.CharField(label="Nom", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    prenom = forms.CharField(label="Prénom", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    sexe = forms.ChoiceField(label="Sexe", choices=[("M", "M"), ("F", "F")])
    mail = forms.CharField(label="E-mail", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    tel = forms.CharField(label="Téléphone", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    adresse = forms.CharField(label="Adresse", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    pbsante = forms.CharField(label="Problème de santé", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    normalien = forms.BooleanField(label="Normalien", required=False)
    section = forms.CharField(label="Section", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))


class Regen_pwForm(forms.Form):
    """Formulaire de demande de nouveau mot de passe"""
    nom = forms.CharField(label="Nom", widget=forms.TextInput(attrs={"autocomplete" : "off"}), required=True)
    prenom = forms.CharField(label="Prénom", widget=forms.TextInput(attrs={"autocomplete" : "off"}), required=True)
    mail = forms.CharField(label="E-mail", widget=forms.TextInput(attrs={"autocomplete" : "off"}), required=True)

class CompteRelatedForm(BaseCompteRelatedForm):
    """Classe de base pour les formulaires traitant un compte avec toutes ses données
       (par opposition à une préinscription)."""
    pseudo = forms.CharField(label="Pseudo", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    fonction = forms.CharField(label="Fonction", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    commentaire = forms.CharField(label="Commentaire", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    report_period = forms.IntegerField(label="Période des rapports (en minutes)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    bloque = forms.BooleanField(label="Bloquer le compte", required=False)
    def __init__(self, *args, **kwargs):
        super(CompteRelatedForm, self).__init__(*args, **kwargs)
        # La section n'est facultative qu'à la préinscription
        self.fields["section"].required = True
    
class CompteForm(CompteRelatedForm):
    """Formulaire pour modifier un compte"""
    def clean(self):
        """Supprime les None des champs facultatifs"""
        out = super(CompteForm, self).clean()
        if out.has_key("report_period") and (out["report_period"] == None):
            del out["report_period"]
        return out

class PreinscriptionForm(BaseCompteRelatedForm):
    """Formulaire de préinscription"""
    def clean(self):
        """Gestion des None"""
        out = super(PreinscriptionForm, self).clean()
        return out

class ReadhesionForm(forms.Form):
    """Formulaire de réadhésion"""
    wei = forms.BooleanField(label="Inscription au WEI", required=False)
    section = forms.CharField(label="Section", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    # partie paiement
    on_note = FrenchFloatField(label="Montant supplémentaire à mettre sur la note", required=False, initial=0, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    type_de_paiement = forms.ChoiceField(label="Type de paiement", choices=[("cheque", "Chèque"), ("especes", "Espèces"), ("note", "Débiter sur note"), ("cb", "Carte bancaire"), ("virement", "Virement bancaire"), ("soge", "Société Générale")])
    pay_nom = forms.CharField(label="Nom¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    pay_prenom = forms.CharField(label="Prénom¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    pay_banque = forms.CharField(label="Banque¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    def clean(self):
        """des valeurs par défaut"""
        out = super(ReadhesionForm, self).clean()
        if out.has_key("on_note") and type(out["on_note"]) == float:
            out["on_note"] = int(round(100 * out["on_note"]))
        else:
            out["on_note"] = 0
        if out.has_key("section") and (out["section"] == ""):
            del out["section"]
        return out


class InscriptionForm(CompteRelatedForm):
    """Formulaire pour inscrire un nouveau compte"""
    wei = forms.BooleanField(label="Inscription au WEI", required=False)
    annee = forms.IntegerField(label="Année d'inscription (année courante si non précisée)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    # partie paiement
    on_note = FrenchFloatField(label="Montant supplémentaire à mettre sur la note", required=False, initial=0, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    type_de_paiement = forms.ChoiceField(label="Type de paiement", choices=[("cheque", "Chèque"), ("especes", "Espèces"), ("cb", "Carte bancaire"), ("virement", "Virement bancaire"), ("soge", "Société Générale")])
    pay_nom = forms.CharField(label="Nom¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    pay_prenom = forms.CharField(label="Prénom¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    pay_banque = forms.CharField(label="Banque¹ (pour le paiement)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    def clean(self):
        """des valeurs par défaut"""
        out = super(InscriptionForm, self).clean()
        if out.has_key("on_note") and type(out["on_note"]) == float:
            out["on_note"] = int(round(100 * out["on_note"]))
        else:
            out["on_note"] = 0
        if out.has_key("report_period") and (out["report_period"] == None):
            del out["report_period"]
        if out.has_key("annee") and (out["annee"] == None):
            del out["annee"]
        return out

class AliasForm(forms.Form):
    """Formulaire pour ajouter un alias"""
    alias = forms.CharField(label="Nouvel alias", widget=forms.TextInput(attrs={"autocomplete" : "off"}))

class PasswordForm(forms.Form):
    """Formulaire pour changer un mot de passe"""
    password = forms.CharField(label="Nouveau mot de passe", widget=forms.PasswordInput)
    password_confirm = forms.CharField(label="Confirmation du mot de passe", widget=forms.PasswordInput, required=True)
    def clean(self):
        """Vérifie que le mot de passe et sa confirmation concordent et enlève le deuxième."""
        out = super(PasswordForm, self).clean()
        if self.errors:
            return out
        if (out["password"] != out["password_confirm"]):
            raise forms.ValidationError("Le mot de passe et sa confirmation ne concordent pas.")
        else:
            del out["password_confirm"]
            return out

class SearchForm(CompteRelatedForm):
    """Formulaire pour faire une recherche avancée"""
    # On peut rechercher sur tous les champs d'un compte, plus les alias et les anciens pseudos
    alias = forms.CharField(label="Alias", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    historique = forms.CharField(label="Ancien pseudo (toujours actif)", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    def __init__(self, *args, **kwargs):
        super(CompteRelatedForm, self).__init__(*args, **kwargs)
        # La section n'est facultative qu'à la préinscription
	for field in self.fields:
            self.fields[field].required = False

class SearchHistoriquePseudoForm(forms.Form):
    """Formulaire pour faire une recherche par ancien pseudo"""
    historique = forms.CharField(label="Ancien pseudo", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    exactfilter = forms.ChoiceField(label="Correspondance sur :",
                                    widget=forms.RadioSelect,
                                    choices=[("b", "début du mot"), ("", "n'importe où dans le mot"), ("x", "correspondance exacte")],
                                    required=False)

# Les formats de timestamps acceptés
#  Les dates
### Les %s sont là parce qu'on a besoin de placer les groupes à deux endroits
### dans la regexp, et ça nécessite qu'ils aient deux noms différents
year_match = ur'(?P<y%s>[0-9]{2}|[0-9]{4})'
ym1, ym2 = year_match % (1), year_match % (2)
month_match = ur'(?P<m%s>[0-9]{1,2})'
mm1, mm2 = month_match % (1), month_match % (2)
day_match = ur'(?P<d%s>[0-9]{1,2})'
dm1, dm2 = day_match % (1), day_match % (2)
date_match = ur'^(?:' + ym1 + '-' + mm1 + '-' + dm1 + '|' + dm2 + '/' + mm2 + '(?:/' + ym2 + ')?)$'

date_matcher = re.compile(date_match)

#  Les heures (il n'est pas nécessaire de les spécifier (défaut à 0 pour chaque champ))
hours_match = ur'(?:(?P<H>[0-9]{1,2}) ?(?:h|heures?)?\.?)?'
minutes_match = ur'(?:(?P<M>[0-9]{1,2}) ?(?:m|min|minutes?)?\.?)?'
seconds_match = ur'(?:(?P<S>[0-9]{1,2}) ?(?:s|sec|secondes?)?\.?)?'
separator_match = u"(?::| )?"
time_match = u'^' + separator_match.join([hours_match, minutes_match, seconds_match]) + u'$'

time_matcher = re.compile(time_match)

def to_string_ignoring_None(obj):
    """Convertit un objet en chaîne de caractères, mais avec None -> '' """
    if (obj == None):
        return ""
    else:
        return unicode(obj)

def to_int_ignoring_null(obj):
    """Convertit un objet en entier mais avec None et '' -> 0"""
    if obj in [None, '']:
        return 0
    else:
        return int(obj)

class MyDateField(forms.CharField):
    """Un champ personnalisé de détection de date.
       Renvoie un objet datetime.date"""
    description = u"Une date (assez souple sur le format d'entrée)"
    
    def to_python(self, raw_value):
        """Conversion de la valeur texte en objet python."""
        raw_value = super(MyDateField, self).to_python(raw_value)
        if raw_value == u'':
            raise forms.ValidationError(u"Ce champ est obligatoire")
        result = date_matcher.match(raw_value)
        if result:
            data = result.groupdict()
            # On commence par fusionner ce qui n'a été séparé que par les règles de re.compile
            for field in ["y", "m", "d"]:
                f1, f2 = field + "1", field + "2"
                data[field] = to_string_ignoring_None(data[f1]) + to_string_ignoring_None(data[f2])
                del data[f1], data[f2]
            # On convertit tout ce beau monde en entier
            for f in data.keys():
                data[f] = to_int_ignoring_null(data[f])
            # Si on n'a pas d'année, on met l'année en cours (plus un si la date se retrouve dans le passé)
            if (data["y"] == 0):
                today = time.localtime()[:3]
                if today[1:] > (data["m"], data["d"]):
                    data["y"] = today[0] + 1
                else:
                    data["y"] = today[0]
            # Si la date a été donnée à deux chiffres, il faut la passer à 4
            if data["y"] < 1000:
                if data["y"] > settings.YEAR_1900s_OVER:
                    data["y"] += 1900
                else:
                    data["y"] += 2000
            # On le renvoie sous forme de datetime.date
            try:
                return datetime.date(data["y"], data["m"], data["d"])
            except Exception as exc:
                raise forms.ValidationError(u'"%s" ne peut pas être transformé en date : "%s"' % (raw_value, exc))
            return data
        else:
            raise forms.ValidationError(u'"%s" ne peut pas être transformé en date (essaye le format JJ/MM/AAAA)' % (raw_value))

class MyTimeField(forms.CharField):
    """Un champ personnalisé de détection d'heure.
       Renvoie un objet datetime.time"""
    description = u"Une heure (assez souple sur le format d'entrée)"
    
    def to_python(self, raw_value):
        """Conversion de la valeur texte en objet python."""
        raw_value = super(MyTimeField, self).to_python(raw_value)
        result = time_matcher.match(raw_value)
        if result:
            data = result.groupdict()
            # On convertit tout ce beau monde en entier
            for f in data.keys():
                data[f] = to_int_ignoring_null(data[f])
            # On le renvoie sous forme de datetime.time
            try:
                return datetime.time(data["H"], data["M"], data["S"])
            except Exception as exc:
                raise forms.ValidationError(u'"%s" ne peut pas être transformé en heure : "%s"' % (raw_value, exc))
        else:
            raise forms.ValidationError(u'"%s" ne peut pas être transformé en heure (essaye le format HH:MM:SS)' % (raw_value))

class ActiviteForm(forms.Form):
    """Formulaire pour créer ou modifer une activité"""
    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    titre = forms.CharField(label="Titre", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    signature = forms.CharField(label="Signature", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    description = forms.CharField(label="Description", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    debut_date = MyDateField(label="Date de début", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    debut_time = MyTimeField(label="Heure de début", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    fin_date = MyDateField(label="Date de fin", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    fin_time = MyTimeField(label="Heure de fin", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    lieu = forms.CharField(label="Lieu", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    liste = forms.BooleanField(label="Liste d'invités", required=False)
    listeimprimee = forms.BooleanField(label="Liste imprimée", required=False)
    def clean(self):
        """Récupère les données et fusionne les dates et les heures
           pour finalement donner une chaîne %Y-%m-%d %H:%M:%S"""
        out = super(ActiviteForm, self).clean()
        # à ce stade, out ne contient pas forcément tous les champs si certains ont été fournis vides
        if not set(["debut_date", "debut_time", "fin_date", "fin_time"]).issubset(out.keys()):
            return out
        dd, dt, fd, ft = out["debut_date"], out["debut_time"], out["fin_date"], out["fin_time"]
        debut, fin = datetime.datetime(dd.year, dd.month, dd.day, dt.hour, dt.minute, dt.second), datetime.datetime(fd.year, fd.month, fd.day, ft.hour, ft.minute, ft.second)
        if debut>fin:
            raise forms.ValidationError("La méthode DeLorean_TimeTravel() n'est pas encore implémentée dans la Note Kfet 2015, merci de réessayer plus tard ou bien de te résoudre à faire commencer ton activité avant qu'elle ne soit finie.")
        else:
            del out["debut_date"], out["debut_time"], out["fin_date"], out["fin_time"]
            out["debut"], out["fin"] = debut.strftime("%Y-%m-%d %H:%M:%S"), fin.strftime("%Y-%m-%d %H:%M:%S")
            return out
    
    def __init__(self, *args, **kwargs):
        """Pour initialiser le formulaire, on doit bidouiller un peu pour avoir
           debut_date, debut_time, fin_date et fin_time dans initial.
           
           Gère aussi les subtilités du champ listeimprimee.
           """
        if 'initial' in kwargs.keys():
            champs = kwargs['initial'].keys()
            for champ_date in ["debut", "fin"]:
                if champ_date in champs:
                    date = time.strptime(kwargs['initial'][champ_date],"%Y-%m-%d %H:%M:%S")
                    # On décompose le timestamp en date/heure
                    kwargs['initial'][champ_date + "_date"], kwargs['initial'][champ_date + '_time'] = time.strftime("%d/%m/%Y %H:%M:%S", date).split(" ")
                    del kwargs['initial'][champ_date]
        # Il faut aussi faire en sorte que le champ "liste imprimée" ne soit pas toujours disponible
        if 'listeimprimee' in kwargs.keys():
            keeplisteimprimee = kwargs['listeimprimee']
            del kwargs['listeimprimee']
        else:
            keeplisteimprimee = True
        forms.Form.__init__(self, *args, **kwargs)
        if not keeplisteimprimee:
            del self.fields['listeimprimee']

class BoutonForm(forms.Form):
    """Formulaire pour créer ou modifier un bouton"""
    label = forms.CharField(label="Label", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    montant = FrenchFloatField(label="Montant", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    destinataire = forms.ChoiceField()
    categorie = forms.ChoiceField()
    affiche = forms.BooleanField(label="Afficher", required=False, initial=True)
    description = forms.CharField(label="Description", widget=forms.TextInput(attrs={"autocomplete" : "off"}), required=False)
    consigne = forms.BooleanField(label="Consigné", required=False, initial=False)
    def __init__(self, *args, **kwargs):
        # Pour initialiser le formulaire, on veut que le solde soit passé en euros
        if 'initial' in kwargs.keys():
            if 'montant' in kwargs['initial'].keys():
                kwargs['initial']['montant'] = kwargs['initial']['montant'] / 100.0
        forms.Form.__init__(self, *args, **kwargs)
    def clean(self):
        """Vérifie que le bouton n'est pas absurde :
           montant >= 0, destinataire et montant ``int``."""
        out = super(BoutonForm, self).clean()
        if out.has_key("montant") and out["montant"] < 0:
            raise forms.ValidationError(u"Le montant d'un bouton doit être positif")
        try:
            out['destinataire'] = int(out['destinataire'])
        except:
            raise forms.ValidationError(u"Je sais pas comme tu t'es débrouillé pour pas me filer un entier dans le champ destinataire, et je veux pas le savoir.")
        try:
            out['montant'] = int(round(100 * out['montant']))
        except:
            raise forms.ValidationError(u"Précise un montant correct.")
        return out

class PhotoForm(forms.Form):
    """Formulaire d'envoi de photo"""
    photo = forms.FileField(label="Fichier photo")
    def clean(self):
        """On n'autorise pas les photos trop grosses."""
        out = super(PhotoForm, self).clean()
        if out.has_key("photo"):
            photo = out["photo"]
            if photo != None and photo.size > settings.MAX_PHOTO_SIZE:
                raise forms.ValidationError(u"Photo trop volumineuse (%s octets), maximum %s" % (photo.size, settings.MAX_PHOTO_SIZE))
        return out

class MoneyForm(forms.Form):
    """Classe de base pour les formulaires qui parlent d'argent."""
    montant = FrenchFloatField(label="Montant", widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    commentaire = forms.CharField(label="Commentaire", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    def clean(self):
        """Gère les conversions en centimes"""
        out = super(MoneyForm, self).clean()
        if out.has_key("montant") and type(out["montant"]) == float:
            out["montant"] = int(round(100 * out["montant"]))
        else:
            out["montant"] = 0
        return out

class CreditRetraitForm(MoneyForm):
    """Formulaire pour effectuer un crédit ou un retrait."""
    idbde = forms.IntegerField(widget=forms.HiddenInput)
    type = forms.ChoiceField(label="Type de paiement", choices=[("", "<choisis un mode de paiement>"), ("especes", "Espèces"), ("cb", "Carte bancaire"), ("cheque", "Chèque"), ("virement", "Virement bancaire")])
    nom = forms.CharField(label="Nom", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    prenom = forms.CharField(label="Prénom", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))
    banque = forms.CharField(label="Banque", required=False)
    def clean(self):
        """Gère le cas où le formulaire a été envoyé sans idbde (l'utilisateur n'a pas cliqué sur une note)
           en levant une erreur spécifique qui sera rattrappée lors de la gestion AJAJ."""
        out = super(CreditRetraitForm, self).clean()
        if out.get("idbde") is None:
            raise CreditRetraitWithoutIdbde
        return out

class TransfertForm(MoneyForm):
    """Formulaire pour effectuer un transfert d'argent.
       Également utilisé pour les dons."""
    commentaire = forms.CharField(label="Motif", required=False, widget=forms.TextInput(attrs={"autocomplete" : "off"}))


class DeleteCompteForm(forms.Form):
    """Formulaire de confirmation de suppression de compte."""
    anonymiser = forms.BooleanField(label="Anonymiser le compte (nom, prénom, mail, adresse, …)", required=False)
