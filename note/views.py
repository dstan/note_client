#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Définitions des views.
   
   Une fonction par page servie.
   """

### Imports
import django

# Modules standard utiles
import random
import random
import json
import base64

# Les objets de réponse HTTP
from django.http import HttpResponse, HttpResponseRedirect
# Pour renvoyer facilement un template
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
# Pour html-escaper, notamment
import django.utils.html

# Les paramètres django
import settings

# Les formulaires
import forms
# Les messages
import messages
# La communication avec le backend
import nk
# Les utilitaires
import utilities
# Des utilitaires pour lesquels on n'a pas envie de rappeler ``module.`` à chaque fois
from utilities import standard_page, standard_page_withignores
# Gestion des requêtes AJAJ
import ajaj

#: Ce module contient les valeurs qu'on a besoin de conserver
#: d'une requête HTTP du client à une autre
import keep_alive

import basic


def login_page(request):
    """Renvoie la page de login ou traite les données du formulaire de login"""
    if request.method == "POST":
        # on récupère le formulaire
        form = forms.LoginForm(request.POST, label_suffix=" :")
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            masque_droits = form.cleaned_data["droits"]
            masque_droits = settings.ACL_MASKS[masque_droits][1]
            # On tente un login
            return nk.login_NK(request, username, password, form, masque_droits)
    else:
        form = forms.LoginForm(label_suffix=" :")
    variables = basic._fundamental_variables()
    variables["form"] = form
    return render(request, "note/login.html", variables)

def index(request):
    """La page qui ne sert à rien"""
    # On peuple les variables, sans demander la socket
    success, sock_ou_response, variables = utilities.get_varsock(request)
    if success:
        return render(request, "note/index.html", variables)
    else:
        # Une erreur a eu lieu
        response = sock_ou_response
        return response

def logout(request):
    """Fonction de déconnexion"""
    if request.session.get("logged", None) == "ok": # Il est possible qu'en fait on ait déjà timeout, donc pas besoin de vraiment se déloguer.
        # On enlève logged du cookie Django
        request.session["logged"] = "no"
        # Il faut fermer la socket
        success, sock_ou_response = nk.socket_still_alive(request)
        if not success:
            return sock_ou_response
        sock = sock_ou_response
        sock.write(json.dumps(["exit"]))
        sock.close()
        # On supprime la socket de keep_alive.CONNS
        idbde = request.session["whoami"]["idbde"]
        del keep_alive.CONNS[idbde]
        # On renvoie sur la page de login
        messages.add_success(request, settings.SUCCMSG_LOGOUT)
    return HttpResponseRedirect(settings.NOTE_LOGIN_URL)

def regen_pw(request, token):
    """Page pour demander l'envoie par mail d'un token de changement de mot de passe
       et pour changer le mot de passe quand on vient avec ce token."""
    variables = {}
    if token:
        if request.method == "POST":
            form = forms.PasswordForm(request.POST, label_suffix=" :")
            if form.is_valid():
                try:
                    sock = nk.connect_NK(request)
                except nk.NKError as exc:
                    return nk.gerer_NKError(request, exc)
                new_pass = form.cleaned_data["password"]
                data = [token, new_pass]
                sock.write(json.dumps(["confirm_reset_password", data]))
                out = nk.full_read(sock)
                errors = out["errmsg"]
                if errors:
                    messages.add_error(request, errors)
                else:
                    messages.add_success(request, out["msg"])
        else:
            form = forms.PasswordForm(label_suffix=" :")
        variables["NOTE_ROOT_URL"] = settings.NOTE_ROOT_URL
        variables["NOTE_LOGIN_URL"] = settings.NOTE_LOGIN_URL
        variables["form"] = form
        return render(request, "note/regen_pw.html", variables)
    else :
        if request.method == "POST":
            form = forms.Regen_pwForm(request.POST, label_suffix=" :")
            if form.is_valid():
                try:
                    sock = nk.connect_NK(request)
                except nk.NKError as exc:
                    return nk.gerer_NKError(request, exc)
                nom = form.cleaned_data["nom"]
                prenom = form.cleaned_data["prenom"]
                mail = form.cleaned_data["mail"]
                data = [prenom,nom,mail]
                sock.write(json.dumps(["generate_reset_password", data]))
                out = nk.full_read(sock)
                errors = out["errmsg"]
                if errors:
                    messages.add_error(request, errors)
                else:
                    messages.add_success(request, out["msg"])
        else:
            form = forms.Regen_pwForm(label_suffix=" :")
        variables["NOTE_ROOT_URL"] = settings.NOTE_ROOT_URL
        variables["NOTE_LOGIN_URL"] = settings.NOTE_LOGIN_URL
        variables["form"] = form
        return render(request, "note/ask_regen_pw.html", variables)


@standard_page
def consos(request, sock, kwargs):
    """La page des consos.
       
       Dans ``kwargs`` :
        * ``double="-double"`` si on a demandé le mode conso double
        """
    variables = {"double_stack_mode" : kwargs["double"], "page_consos" : True}
    categories = utilities._get_boutons_categories(sock, request)
    variables["categories"] = categories
    boutons = utilities._get_boutons(sock, request)
    variables["boutons"] = [(categ, [b for b in boutons if b["categorie"] == categ]) for categ in categories]
    sock.write(json.dumps(["historique_transactions", "last"]))
    out = nk.full_read(sock)
    if nk._is_success_code(out["retcode"]):
        variables["historique"] = out["msg"]
    else:
        messages.add_error(request, out["errmsg"])
    sock.write(json.dumps(["mayi", "transferts"]))
    variables["hastransfert"] = nk.full_read(sock)["msg"]
    sock.write(json.dumps(["mayi", "credits"]))
    variables["hascredit"] = nk.full_read(sock)["msg"]
    sock.write(json.dumps(["mayi", "retraits"]))
    variables["hasretrait"] = nk.full_read(sock)["msg"]
    # Le formulaire de Crédit
    variables["credit_form"] = forms.CreditRetraitForm(prefix="credit_form", label_suffix=" :")
    # Le formulaire de Retrait
    variables["retrait_form"] = forms.CreditRetraitForm(prefix="retrait_form", label_suffix=" :")
    # Le formulaire de Transfert
    variables["transfert_form"] = forms.TransfertForm(prefix="transfert_form", label_suffix=" :")
    return (variables, "note/consos.html")

@standard_page
def dons(request, sock, kwargs):
    """La page des dons"""
    variables = {}
    # Le formulaire de Don
    variables["don_form"] = forms.TransfertForm(prefix='transfert_form', label_suffix=" :")
    return (variables, "note/dons.html")

@standard_page
def activites(request, sock, kwargs):
    """Affichage des activités
       
       Dans ``kwargs`` :
        * ``admin`` qui vaut ``"/admin"`` si on a demandé le mode admin
        """
    admin = kwargs["admin"]
    asked_admin = (admin == "/admin")
    # on demande si on le droit d'être admin
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    # on est en mode administration si on en a le droit ET qu'on l'a demandé
    isadmin = asked_admin and hasadmin
    if request.method == "POST":
        return HttpResponse("Bad Request", status=400)
    else:
        liste_activites = utilities._get_activites(sock, isadmin, request)
        # On affiche la liste des activités en ajoutant les variables standard
        variables = {"activites": liste_activites,
                     "hasadmin": hasadmin,
                     "isadmin": isadmin}
        return (variables, "note/activites.html")

@standard_page
def activite(request, sock, kwargs):
    """Affichage d'une activité pour y inviter
       
       Dans ``kwargs`` :
        * ``idact``
        * ``admin`` qui vaut ``"/admin"`` si on a demandé le mode admin
        """
    idact, admin = kwargs["idact"], kwargs["admin"]
    asked_admin = (admin == "/admin")
    # On demande si on le droit d'être admin
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    # On est en mode administration si on en a le droit ET qu'on l'a demandé
    isadmin = asked_admin and hasadmin
    # On récupère l'activité
    activite = utilities._get_activite(sock, idact, request)
    if request.method == "POST":
        form = forms.InviteForm(request.POST, label_suffix=" :")
        if form.is_valid():
            nom = form.cleaned_data["nom"]
            prenom = form.cleaned_data["prenom"]
            data = [nom, prenom, idact]
            if isadmin: # un admin doit préciser le reponsable (en cas d'échec ce sera lui le responsable)
                try:
                    data.append(int(request.POST["idrespo"]))
                except:
                    pass
            sock.write(json.dumps(["add_invite", [data, "A" * isadmin]]))
            out = nk.full_read(sock)
            erreur = out["errmsg"]
            if erreur:
                if out["retcode"] == 110:
                    messages.add_warning(request, erreur)
                else:
                    messages.add_error(request, erreur)
            else:
                messages.add_success(request, settings.SUCCMSG_ADDINV)
                return HttpResponseRedirect("%sactivites/%s%s/" % (settings.NOTE_ROOT_URL, idact, "/admin" * isadmin))

    else:
        form = forms.InviteForm(label_suffix=" :", initial=activite)
    liste_invites = utilities._get_invites(sock, idact, isadmin, request)
    # on prépare les variables
    variables = {"activite": activite,
                 "form": form,
                 "liste_invites": liste_invites,
                 "hasadmin": hasadmin,
                 "isadmin": isadmin}
    return (variables, "note/invitation.html")

@standard_page
def activite_gestion(request, sock, kwargs):
    """Page de gestion d'une activité.
       
       Dans ``kwargs`` :
        * ``idact`` : n° de l'activité à gérer
        * ``validation`` : fin de l'url, peut être ``"/validate"``, ``"/invalidate"`` ou ``/delete``
       """
    validation, idact = kwargs["validation"], kwargs["idact"]
    variables_standard = kwargs["variables_standard"]
    if (request.method == "GET") and (validation == "/delete"):
        # suppression de l'activité effectuée par une fonction dédiée
        utilities._del_activite(sock, request, idact)
        return HttpResponseRedirect('%sactivites/' % (settings.NOTE_ROOT_URL,))
    activite = utilities._get_activite(sock, idact, request, computecandelete=True, whoami=variables_standard["whoami"], isadmin=True)
    variables = {}
    variables["activite"] = activite
    if validation == "/validate":
        action = "valider_activite"
    elif validation == "/invalidate":
        action = "devalider_activite"
    else:
        action = None
    if action:
        sock.write(json.dumps([action, idact]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            if out["retcode"] == 0:
                succmsg = settings.SUCCMSG_VALIDACT if (validation == "/validate") else settings.SUCCMSG_DEVALIDACT
                messages.add_success(request, succmsg)
            else:
                messages.add_warning(request, out["errmsg"])
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect('%sactivites/%s/gestion/' % (settings.NOTE_ROOT_URL, idact))
    return (variables, "note/activite_gestion.html")

@standard_page
def activite_gestion_modifier(request, sock, kwargs):
    """Page pour voir/éditer une activité, en tant qu'admin
       
       Dans ``kwargs`` :
        * ``idact`` : n° de l'activité à modifier
        """
    idact = kwargs["idact"]
    variables = {}
    activite = utilities._get_activite(sock, idact, request)
    variables["activite"] = activite
    if request.method == "GET":
        form = forms.ActiviteForm(label_suffix=" :", initial=activite)
        variables["form"] = form
        return (variables, "note/activite_modifier.html")
    else:
        form = forms.ActiviteForm(request.POST, label_suffix=" :")
        variables["form"] = form
        if form.is_valid():
            keysact = activite.keys()
            # on regarde les champs qui sont différents
            actdata = {k: v for (k, v) in form.cleaned_data.items() if k in keysact and (v != activite[k])}
            if actdata != {}:
                # On rajoute l'idact
                actdata["id"] = idact
                # On demande toujours à faire l'update en tant qu'admin
                #  de toutes façon ça ne provoque pas d'erreur si on ne l'est pas
                tosend = [actdata, "A"]
                sock.write(json.dumps(["update_activite", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, settings.SUCCMSG_CHGACT)
                else:
                    messages.add_error(request, out["errmsg"])
                    return (variables, "note/activite_modifier.html")
            # on renvoie sur la page de visualisation de l'activité modifiée
            return HttpResponseRedirect("%sactivites/%s/gestion/" % (settings.NOTE_ROOT_URL, idact))
        else:
            return (variables, "note/activite_modifier.html")

@standard_page_withignores(["idact"])
def mes_activites(request, sock, kwargs):
    """Page "Mes Activités" (ajout, modification, suppression si non encore validée)
       
       Dans ``kwargs`` :
        * ``idact`` si on modifie une activité
        * ``delete="/delete"`` si on supprime une activité
        """
    idact, delete = kwargs["idact"], kwargs["delete"]
    variables_standard = kwargs["variables_standard"]
    variables = {}
    # on demande si on le droit d'être admin
    sock.write(json.dumps(["mayi", "activites_admin"]))
    hasadmin = nk.full_read(sock)["msg"]
    variables["hasadmin"] = hasadmin 
    if idact is None:
        mes_activites = utilities._get_activites(sock, False, request, computecandelete=True, whoami=variables_standard["whoami"], mine=True)
        variables["activites"] = mes_activites
        if request.method == "POST":
            form = forms.ActiviteForm(request.POST, label_suffix=" :", listeimprimee=False)
            if form.is_valid():
                actdata = form.cleaned_data
                del actdata["id"]
                sock.write(json.dumps(["add_activite", actdata]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, settings.SUCCMSG_ADDACT)
                    return HttpResponseRedirect('%smes_activites/' % (settings.NOTE_ROOT_URL,))
                else:
                    messages.add_error(request, out["errmsg"])
        else:
            form = forms.ActiviteForm(label_suffix=" :", listeimprimee=False)
        variables["form"] = form
        return (variables, "note/mes_activites.html")
    else:
        if delete == "/delete":
            succeed = utilities._del_activite(sock, request, idact)
            return HttpResponseRedirect('%smes_activites/' % (settings.NOTE_ROOT_URL,))
        activite = utilities._get_activite(sock, idact, request, fallback='%smes_activites/' % (settings.NOTE_ROOT_URL,))
        variables["activite"] = activite
        if request.method == "GET":
            form = forms.ActiviteForm(label_suffix=" :", initial=activite, listeimprimee=False)
        else:
            form = forms.ActiviteForm(request.POST, label_suffix=" :", listeimprimee=False)
            if form.is_valid():
                actdata = {champ: valeur for (champ, valeur) in form.cleaned_data.items() if (champ == "id") or (valeur != activite[champ])}
                sock.write(json.dumps(["update_activite", [actdata, "A"]]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, settings.SUCCMSG_CHGACT)
                    return HttpResponseRedirect('%smes_activites/' % (settings.NOTE_ROOT_URL,))
                else:
                    messages.add_error(request, out["errmsg"])
        variables["form"] = form
        return (variables, "note/activite_modifier.html")

@standard_page
def del_invite(request, sock, kwargs):
    """Suppression d'un invité
       
       Dans ``kwargs`` :
        * ``idact`` : l'activité dont on veut retirer l'invité
        * ``idinv`` : l'invité qu'on veut retirer
        * ``admin="/admin"`` si on est mode administration"""
    variables = {}
    idact, idinv = kwargs["idact"], kwargs["idinv"]
    admin = (kwargs["admin"] == "/admin")
    if idinv != -1:
        # On demande toujours la suppression avec le flag "A",
        # de toutes façons il est ignoré par le serveur si on n'a pas les droits
        sock.write(json.dumps(["del_invite", [idinv, "A"]]))
        out = nk.full_read(sock)
        if out["retcode"] == 404:
            messages.add_error(request, settings.ERRMSG_IDINV_FAIL % (idinv))
        elif nk._is_success_code(out["retcode"]):
            messages.add_success(request, settings.SUCCMSG_DELINV)
        else:
            messages.add_error(request, out["errmsg"])
    return HttpResponseRedirect(u"%sactivites/%s/%s" % (settings.NOTE_ROOT_URL, idact, "admin/" * admin))

@standard_page
def toggle_transaction(request, sock, kwargs):
    """Valider/Dévalider une transaction.
       
       Dans ``kwargs`` :
        * ``idtransaction`` : id de la transaction à toggle
        * ``de`` = ``"de"`` si on veut dévalider, ``""`` sinon.
        """
    devalidate = (kwargs["de"] == "de")
    idtransaction = kwargs["idtransaction"]
    variables = {}
    cmd = "%svalider_transaction" % ("de" if devalidate else "")
    sock.write(json.dumps([cmd, idtransaction]))
    out = nk.full_read(sock)
    if out["retcode"] == 404:
        messages.add_error(request, settings.ERRMSG_IDTRANSACTION_FAIL % (idinv))
    elif nk._is_success_code(out["retcode"]):
        succmsg = settings.SUCCMSG_DEVALIDATE_TRANSACTION if devalidate else settings.SUCCMSG_VALIDATE_TRANSACTION
        messages.add_success(request, succmsg)
    else:
        messages.add_error(request, out["errmsg"])
    redirect = request.GET.get("coming_from", u"%sconsos/" % settings.NOTE_ROOT_URL)
    return HttpResponseRedirect(redirect)

@standard_page_withignores(["idbde"])
def comptes(request, sock, kwargs):
    """La page de recherche d'un compte ou qui l'affiche.
       
       Dans ``kwargs`` :
        * ``idbde`` si on affiche un compte
        """
    idbde = kwargs["idbde"]
    if request.method == "GET":
        if idbde is None:
            return ({}, "note/comptes.html")
        else:
            variables = utilities._prepare_variables(sock, idbde, request)
            return (variables, "note/un_compte.html")
    else:
        return HttpResponse("Méthode invalide", status=400)

@standard_page_withignores(["idbde"])
def readhesions(request, sock, kwargs):
    """La page de recherche d'un compte ou qui l'affiche.
       
       Dans ``kwargs`` :
        * ``idbde`` si on affiche un compte
        """
    idbde = kwargs["idbde"]
    sock.write(json.dumps(["get_tarifs_adhesion"]))
    tarifs = nk.full_read(sock)["msg"]
    if request.method == "GET":
        if idbde is None:
            return ({}, "note/readhesions.html")
        else:
            variables = utilities._prepare_variables(sock, idbde, request)
            variables.update(tarifs)
            form = forms.ReadhesionForm(label_suffix=" :",initial={"pay_nom" : variables["compte"]["nom"],"pay_prenom" : variables["compte"]["prenom"]})
            variables["form"] = form
            return (variables, "note/une_readhesion.html")
    else:
        form = forms.ReadhesionForm(request.POST, label_suffix=" :")
        if form.is_valid():
            readhdata = form.cleaned_data
            # Il faut formater ça correctement pour l'envoyer au serveur NK
            pay = {"type": readhdata["type_de_paiement"], "montant" : readhdata["on_note"],
                    "nom": readhdata["pay_nom"], "prenom": readhdata["pay_prenom"], "banque": readhdata["pay_banque"]}
            # On vire les champs en question du dico
            for champ in ["on_note", "type_de_paiement", "pay_nom", "pay_prenom", "pay_banque"]:
                del readhdata[champ]
            # On cherche à savoir combien lui coûte l'adhésion
            if readhdata["wei"]:
                # On a alors besoin de savoir si il est normalien ou non
                sock.write(json.dumps(["compte", idbde]))
                compte = nk.full_read(sock)["msg"]
                if compte["normalien"]:
                    tarif = tarifs["prix_wei_normalien"]
                else:
                    tarif = tarifs["prix_wei_non_normalien"]
            else:
                tarif = tarifs["prix_adhesion"]
            pay["montant"] += tarif
            readhdata["pay"] = pay
            readhdata["idbde"] = idbde
            sock.write(json.dumps(["readherer", readhdata]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_READHESION)
                return ({}, "note/readhesions.html")
            else:
                messages.add_error(request, out["errmsg"])
                return HttpResponseRedirect("%sreadhesions/%s/" % (settings.NOTE_ROOT_URL, idbde))


@standard_page
def historique_transactions(request, sock, kwargs):
    """Page de l'historique des transactions d'un compte.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut l'historique des transactions.
        """
    idbde = kwargs["idbde"]
    if idbde is None:
        return HttpResponseRedirect('%scomptes/' % (settings.NOTE_ROOT_URL,))
    variables = {"idbde" : idbde}
    sock.write(json.dumps(["historique_transactions", idbde]))
    out = nk.full_read(sock)
    if nk._is_success_code(out["retcode"]):
        variables['historique'] = out["msg"]
    else:
        messages.add_error(request, out["errmsg"])
    return (variables, "note/un_compte_historique.html")

@standard_page
def comptes_advanced(request, sock, kwargs):
    """Page de recherche avancée"""
    variables = {}
    # On cherche si on a le droit full_search
    sock.write(json.dumps(["mayi", "full_search"]))
    mayi = nk.full_read(sock)["msg"]
    variables["acl_full_search"] = mayi
    # certains champs ne seront accessibles qu'aux utilisateurs ayant les droits full_search
    variables["full_search_fields"] = ["tel", "adresse", "pbsante"]
    if request.method == "GET":
        form = forms.SearchForm(label_suffix=" :")
        variables["form"] = form
        variables["search_flags"] = "io" # par défaut, la recherche est case-insensitive et sur les comptes non à jour
        variables["exactfilter"] = "b" # par défaut, on matche sur le début du mot
    else:
        form = forms.SearchForm(request.POST, label_suffix=" :")
        checked_fields = [champ[4:] for (champ, valeur) in request.POST.items() if (champ[:4] == "box_") and (valeur == "on")]
        variables["checked_fields"] = checked_fields
        exactfilter = request.POST["exactfilter"]
        variables["exactfilter"] = exactfilter
        search_flags_dico = {"search_alias": "A",
                             "give_alias": "a",
                             "search_historique": "H",
                             "give_historique": "h",
                             "case_insensitive": "i",
                             "old_accounts": "o"}
        search_flags = "".join([search_flags_dico.get(champ, "") for champ in request.POST.keys()])
        if form.is_valid():
            # On récupère toutes les données sur lesquelles on doit chercher
            searching = {champ: form.cleaned_data[champ] for champ in checked_fields}
            searchfields = searching.keys()
            if "alias" in searchfields:
                search_flags += "A"
            if "historique" in searchfields:
                search_flags += "H"
            variables["search_flags"] = search_flags
            flags = exactfilter + search_flags
            tosend = [flags, searching]
            sock.write(json.dumps(["search", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                # les champs alias et historiques ont déjà été gentiments post-processés par le serveur
                # mais quand il n'y en a pas, on récupère null, pythonisé en None, et c'est moche
                liste = out["msg"]
                for i in range(len(liste)):
                    for champ in ["aliases", "historiques"]:
                        if liste[i].get(champ, "") == None:
                            liste[i][champ] = u""
                variables["search_result"] = liste
                variables["give_alias"] = ("a" in search_flags)
                variables["give_historique"] = ("h" in search_flags)
                return (variables, "note/recherche_avancee_resultats.html")
            else:
                messages.add_error(request, out["errmsg"])
                variables["form"] = form
        else:
            variables["form"] = form
    return (variables, "note/recherche_avancee.html")

@standard_page
def modifier_compte(request, sock, kwargs):
    """Page de modification de compte.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte à modifier
        """
    idbde = kwargs["idbde"]
    variables_standard = kwargs["variables_standard"]
    variables = utilities._prepare_variables(sock, idbde, request, form=True, whoami=variables_standard["whoami"])
    if request.method == "GET":
        if variables.has_key("compte"):
            form = forms.CompteForm(initial=variables["compte"])
            variables["form"] = form
            return (variables, "note/modifier_compte.html")
        else:
            return HttpResponseRedirect("%scomptes/" % (settings.NOTE_ROOT_URL,))
    elif request.method == "POST":
        # on a envoyé un formulaire de modification du compte
        form = forms.CompteForm(request.POST)
        if form.is_valid():
            fields = form.cleaned_data
            gotcompte = variables.has_key("compte")
            if not gotcompte:
                return HttpResponseRedirect("%scomptes/" % (settings.NOTE_ROOT_URL,))
            compte = variables["compte"]
            keyscompte = compte.keys()
            # on regarde les champs qui sont différents
            tosend = {k: v for (k, v) in form.cleaned_data.items() if k in keyscompte and (v != compte[k])}
            # les droits ça se gère un peu spécialement
            # avec des virgules et des cases à cocher
            for champ in ["droits", "surdroits"]:
                n = len(champ) + 1
                droitsousur = [case[n:] for (case, valeur) in request.POST.items() if case.startswith("%s_" % champ) and (valeur == "on")]
                liste = compte[champ]
                if set(droitsousur) != set(liste):
                    tosend[champ] = u",".join(droitsousur)
            if tosend != {}: # si il y a vraiment quelque chose à faire
                # on rajoute l'idbde
                tosend["idbde"] = idbde
                # si on n'a pas les droits wei (et que ce n'est pas notre compte)
                #  on ne tient pas compte du contenu de "numsecu" et "pbsante"
                if not variables["has_wei"]:
                    if tosend.has_key("pbsante"):
                        del tosend["pbsante"]
                sock.write(json.dumps(["update_compte", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, settings.SUCCMSG_ACCOUNT_CHANGED)
                else:
                    messages.add_error(request, out["errmsg"])
                    variables["form"] = form
                    return (variables, "note/modifier_compte.html")
            # on renvoie sur la page de visualisation du compte modifié
            return HttpResponseRedirect("%scomptes/%s/" % (settings.NOTE_ROOT_URL, idbde))
        else:
            variables["form"] = form
            return (variables, "note/modifier_compte.html")

@standard_page
def supprimer_compte(request, sock, kwargs):
    """Page de confirmation de suppression de compte.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte à supprimer
        """
    idbde = kwargs["idbde"]
    variables = {"button_class" : "btn-danger", "button_content" : u"Confirmer la suppression"}
    variables_standard = kwargs["variables_standard"]
    variables.update(utilities._prepare_variables(sock, idbde, request, form=True, whoami=variables_standard["whoami"]))
    if request.method == "GET":
        if variables.has_key("compte"):
            form = forms.DeleteCompteForm(initial=variables["compte"])
            variables["form"] = form
            return (variables, "note/supprimer_compte.html")
        else:
            return HttpResponseRedirect("%scomptes/" % (settings.NOTE_ROOT_URL,))
    elif request.method == "POST":
        # on a envoyé un formulaire de suppression du compte
        form = forms.DeleteCompteForm(request.POST)
        if form.is_valid():
            fields = form.cleaned_data
            if not variables.has_key("compte"):
                return HttpResponseRedirect("%scomptes/" % (settings.NOTE_ROOT_URL,))
            compte = variables["compte"]
            # on regarde les champs qui sont différents
            tosend = [idbde, fields["anonymiser"]]
            sock.write(json.dumps(["supprimer_compte", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_ACCOUNT_DELETED)
            else:
                messages.add_error(request, out["errmsg"])
                variables["form"] = form
                return (variables, "note/supprimer_compte.html")
            # on renvoie sur la page de visualisation du compte modifié
            return HttpResponseRedirect("%scomptes/%s/" % (settings.NOTE_ROOT_URL, idbde))
        else:
            variables["form"] = form
            return (variables, "note/supprimer_compte.html")

@standard_page
def update_photo(request, sock, kwargs):
    """La page de modification des photos.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut modifier la photo
        """
    idbde = kwargs["idbde"]
    variables = {}
    if request.method == "POST":
        form = forms.PhotoForm(request.POST, request.FILES, label_suffix=" :")
        if form.is_valid():
            photo = request.FILES["photo"]
            # On envoie la photo au serveur NK
            photodata = photo.read()
            photob64 = base64.b64encode(photodata)
            format = photo.name.rsplit('.',1)[-1]
            sock.write(json.dumps(["update_photo", [idbde, photob64, format]]))
            answer = nk.full_read(sock)
            if nk._is_success_code(answer["retcode"]):
                messages.add_success(request, settings.SUCCMSG_PHOTO_UPDATED)
                return HttpResponseRedirect('%scomptes/%s/' % (settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, answer["errmsg"])
    else:
        form = forms.PhotoForm(label_suffix=" :")
    variables["idbde"] = idbde
    variables["form"] = form
    return (variables, "note/un_compte_photo.html")

@standard_page
def historique_pseudo(request, sock, kwargs):
    """La page de visualisation de l'historique des pseudos.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte ont on veut l'historique des pseudos
       """
    idbde = kwargs["idbde"]
    variables = {}
    if request.method == "GET":
        compte = utilities._get_historique_pseudo(sock, idbde, request)
        variables["compte"] = compte
        variables["historique_pseudo"] = compte["historique_pseudo"]
        return (variables, "note/historique_pseudo.html")
    else:
        return HttpResponse("Bad request method : %s" % (request.method))

@standard_page
def search_historique_pseudo(request, sock, kwargs):
    """Page de recherche par ancien pseudo (même inactif)."""
    variables = {}
    if request.method == "GET":
        form = forms.SearchHistoriquePseudoForm(label_suffix=" :", initial={"exactfilter": "b"})
        variables["form"] = form
    else:
        form = forms.SearchHistoriquePseudoForm(request.POST, label_suffix=" :")
        if form.is_valid():
            tosend = [form.cleaned_data["historique"], form.cleaned_data["exactfilter"]]
            sock.write(json.dumps(["search_historique_pseudo", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                variables["is_displaying"] = True
                liste = out["msg"]
                variables["search_result"] = liste
            else:
                messages.add_error(request, out["errmsg"])
        else:
            variables["form"] = form
    return (variables, "note/search_historique_pseudo.html")

@standard_page
def aliases(request, sock, kwargs):
    """La page de gestion des aliases.
       
       Dans ``kwargs`` :
        * ``idbde`` : l'id du compte
        """
    idbde = kwargs["idbde"]
    variables = {}
    compte = utilities._get_aliases(sock, idbde, request)
    variables["compte"] = compte
    if request.method == "GET":
        form = forms.AliasForm(label_suffix=" :")
    else:
        form = forms.AliasForm(request.POST, label_suffix=" :")
        if form.is_valid():
            alias = form.cleaned_data["alias"]
            tosend = [idbde, alias]
            sock.write(json.dumps(["alias", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_ALIAS_ADDED)
                return HttpResponseRedirect("%scomptes/%s/aliases/" % (settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, out["errmsg"])
    variables["form"] = form
    return (variables, "note/aliases.html")

@standard_page
def unalias(request, sock, kwargs):
    """Fonction appelée lorsqu'on veut supprimer un/des alias.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut supprimer un/des alias
        * ``delall="_all"`` si on veut supprimer tous les alias du compte (prioritaire)
        * ``idalias`` : id d'un alias à supprimer
        """
    variables = {}
    idbde, delall, idalias = kwargs["idbde"], kwargs["delall"], kwargs["idalias"]
    if request.method == "GET":
        delete_all = (delall == "_all")
        if delete_all:
            sock.write(json.dumps(["unalias", [idbde, True]]))
        else:
            try:
                idalias = int(idalias)
            except:
                messages.add_error(request, settings.ERRMSG_IDALIAS_INVALID % (idalias))
                return HttpResponseRedirect("%scomptes/%s/aliases/" % (settings.NOTE_ROOT_URL, idbde))
            sock.write(json.dumps(["unalias", [idalias, False]]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            if delete_all:
                msg = settings.SUCCMSG_ALIAS_ALLDELETED
            else:
                msg = settings.SUCCMSG_ALIAS_DELETED
            messages.add_success(request, msg)
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect("%scomptes/%s/aliases/" % (settings.NOTE_ROOT_URL, idbde))
    else:
        return HttpResponse("Bad request method : %s" % (request.method))

@standard_page
def password(request, sock, kwargs):
    """Page de changement de mot de passe.
       
       Dans ``kwargs`` :
        * ``idbde`` : id du compte dont on veut changer le mot de passe
        """
    idbde = kwargs["idbde"]
    variables = {}
    if request.method == "GET":
        compte = utilities._get_compte(sock, idbde, request)
        variables["compte"] = compte
        if kwargs["variables_standard"]["whoami"]["idbde"] == idbde:
            # On peut changer son propre mot de passe
            variables["form"] = forms.PasswordForm(label_suffix=" :")
        elif idbde <= 0:
            messages.add_error(request, settings.ERRMSG_PASSWORD_NEGATIVE_IDBDE)
        else:
            # Il faut alors vérifier si il a le droit chgpass
            sock.write(json.dumps(["mayi", "chgpass"]))
            out = nk.full_read(sock)["msg"]
            if out == True:
                variables["form"] = forms.PasswordForm(label_suffix=" :")
            else:
                messages.add_error(request, settings.ERRMSG_NO_ACL_CHGPASS)
        return (variables, "note/password.html")
    else:
        form = forms.PasswordForm(request.POST, label_suffix=" :")
        if form.is_valid():
            newpass = form.cleaned_data["password"]
            tosend = [idbde, newpass]
            sock.write(json.dumps(["chgpass", tosend]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_PASSWORD_CHANGED)
                return HttpResponseRedirect("%scomptes/%s/" % (settings.NOTE_ROOT_URL, idbde))
            else:
                messages.add_error(request, out["errmsg"])
                variables["form"] = form
                return (variables, "note/password.html")
        else:
            compte = utilities._get_compte(sock, idbde, request)
            variables["compte"] = compte
            variables["form"] = form
            return (variables, "note/password.html")


def _create_BoutonForm(sock, request, contenu=None, initial=None):
    """Un hack pour peupler au runtime
       les choices du champ destinataire d'un formulaire de création de bouton."""
    if initial == None:
        if contenu == None:
            form = forms.BoutonForm(label_suffix=" :")
        else:
            form = forms.BoutonForm(contenu, label_suffix=" :")
    else:
        form = forms.BoutonForm(label_suffix=" :", initial=initial)
    categories = utilities._get_boutons_categories(sock, request)
    form.fields['categorie'].choices = [[ca] * 2 for ca in categories]
    clubs = utilities._get_clubs(sock, request)
    form.fields['destinataire'].choices = [[cl["idbde"], cl["pseudo"]] for cl in clubs]
    return form

@standard_page
def boutons(request, sock, kwargs):
    """Page de gestion des boutons.
       
       Dans ``kwargs`` :
        * ``idbouton`` : id du bouton
        * ``delete="/delete"`` si on cherche à le supprimer
        """
    variables = {}
    idbouton = kwargs.get("idbouton")
    delete = kwargs.get("delete")
    if delete == "/delete":
        sock.write(json.dumps(["delete_bouton", idbouton]))
        out = nk.full_read(sock)
        if nk._is_success_code(out["retcode"]):
            messages.add_success(request, settings.SUCCMSG_DELBUTTON)
        else:
            messages.add_error(request, out["errmsg"])
        return HttpResponseRedirect('%sboutons/' % (settings.NOTE_ROOT_URL,))
    if request.method == "GET":
        if idbouton == None:
            boutons = utilities._get_boutons(sock, request, hidden=True)
            variables["boutons"] = boutons
            # le formulaire est vide
            form = _create_BoutonForm(sock, request)
        else:
            bouton = utilities._get_un_bouton(sock, idbouton, request, fallback='%sboutons/' % (settings.NOTE_ROOT_URL,))
            variables["un_bouton"] = bouton
            # le formulaire contient les données du bouton
            form = _create_BoutonForm(sock, request, initial=bouton)
        variables["form"] = form
    else:
        form = _create_BoutonForm(sock, request, contenu=request.POST)
        if form.is_valid():
            if idbouton == None: # c'est donc un ajout
                sock.write(json.dumps(["create_bouton", form.cleaned_data]))
            else: # c'est une modification
                data = form.cleaned_data
                data["id"] = idbouton
                sock.write(json.dumps(["update_bouton", data]))
            out = nk.full_read(sock)
            retcode = out["retcode"]
            if retcode == 103:
                # C'est le code "Ce bouton existe déjà, c'est pas la peine de l'ajouter"
                messages.add_success(request, out["errmsg"])
                return HttpResponseRedirect("%sboutons/" % (settings.NOTE_ROOT_URL,))
            elif nk._is_success_code(retcode):
                if idbouton == None: #ajout
                    messages.add_success(request, settings.SUCCMSG_ADDBUTTON)
                else: #modification
                    messages.add_success(request, settings.SUCCMSG_CHGBUTTON)
                return HttpResponseRedirect("%sboutons/" % (settings.NOTE_ROOT_URL,))
            else:
                messages.add_error(request, out["errmsg"])
        else:
            if idbouton == None:
                boutons = utilities._get_boutons(sock, request)
                variables["boutons"] = boutons
            else:
                # on est dans le cas : je poste pour modifier un bouton, mais j'ai échoué
                # on a besoin de la présence de la variable un_bouton mais on n'a pas besoin d'aller le chercher
                # on a juste besoin de son id
                variables["un_bouton"] = {"id": idbouton}
            variables["form"] = form
    return (variables, "note/boutons.html")

@standard_page
def preinscriptions(request, sock, kwargs):
    """Page de gestion des préinscriptions."""
    variables = {}
    if request.method == "GET":
        form = forms.PreinscriptionForm(label_suffix=" :")
    else:
        form = forms.PreinscriptionForm(request.POST, label_suffix=" :")
        if form.is_valid():
            preinsdata = form.cleaned_data
            sock.write(json.dumps(["preinscrire", preinsdata]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_PREINSCRIPTION_ADDED)
                # on ne fait pas un redirect, mais on vide le formulaire
                form = forms.PreinscriptionForm(label_suffix=" :")
            else:
                messages.add_error(request, out["errmsg"])
    variables["form"] = form
    return (variables, "note/preinscriptions.html")

@standard_page_withignores(["preid"])
def inscriptions(request, sock, kwargs):
    """Page de gestion des inscriptions.
       
       Dans ``kwargs`` :
        * ``preid`` : id de la préinscription dont on s'occupe
        * ``delete="/delete"`` si on cherche à la supprimer
        """
    variables = {}
    preid, delete = kwargs["preid"], kwargs["delete"]
    if preid != None:
        preins = utilities._get_preinscription(sock, preid, request)
        if delete == "/delete":
            sock.write(json.dumps(["del_preinscription", preid]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                messages.add_success(request, settings.SUCCMSG_PREINSCRIPTION_DELETED)
            else:
                messages.add_error(request, out["errmsg"])
            return HttpResponseRedirect('%sinscriptions/' % (settings.NOTE_ROOT_URL,))
        variables["preinscription"] = preins
        if request.method == "POST":
            form = forms.InscriptionForm(request.POST, label_suffix=" :")
            if form.is_valid():
                inscription = form.cleaned_data
                # Il faut formater ça correctement pour l'envoyer au serveur NK
                pay = [inscription["on_note"], inscription["type_de_paiement"],
                        {"nom": inscription["pay_nom"], "prenom": inscription["pay_prenom"], "banque": inscription["pay_banque"]}]
                # On vire les champs en question du dico
                for champ in ["on_note", "type_de_paiement", "pay_nom", "pay_prenom", "pay_banque"]:
                    del inscription[champ]
                tosend = [preid, inscription, pay]
                sock.write(json.dumps(["inscrire", tosend]))
                out = nk.full_read(sock)
                if nk._is_success_code(out["retcode"]):
                    messages.add_success(request, settings.SUCCMSG_ACCOUNT_ADDED)
                    return HttpResponseRedirect('%sinscriptions/' % (settings.NOTE_ROOT_URL,))
                else:
                    messages.add_error(request, out["errmsg"])
        else:
            # on rajoute dans le champ pseudo la valeur par défaut
            sock.write(json.dumps(["get_default_pseudo", [preins["nom"], preins["prenom"]]]))
            out = nk.full_read(sock)
            if nk._is_success_code(out["retcode"]):
                preins["pseudo"] = out["msg"]
            else:
                messages.add_error(request, out["errmsg"])
            form = forms.InscriptionForm(label_suffix=" :", initial=preins)
        variables["form"] = form
    else:
        liste_preinscriptions = utilities._get_preinscriptions(sock, request)
        variables["liste_preinscriptions"] = liste_preinscriptions
    return (variables, "note/inscriptions.html")

def teapot(request):
    """Easter egg"""
    page = """<html><head>\n<title>418 I'm a Teapot</title>\n</head><body>\n<h1>HTCPCP error 418: I'm a Teapot</h1>\nThe requested URL cannot provide coffe, because this is a networked teapot. See RFC2324.<p>\n<hr>\n"""
    page += _display_versions()
    page += "</html>\n</body></html>"
    return HttpResponse(page, status=418)

def page_not_found(request):
    """Page 404 customisée pour y ajouter des variables."""
    variables = {"report_bugs_to" : settings.REPORT_BUGS_EMAIL,
                 "NOTE_ROOT_URL" : settings.NOTE_ROOT_URL}
    return render(request, "404.html", variables)
