#!/usr/bin/python
# -*- encoding: utf-8 -*-

""" Attention, ce module n'est pas la vraie version utilisée par le projet.
    C'est une version de présentation incluse dans le dépôt git
    à adapter et à renommer en enlevant le ``_sample``
    
    À remplir avec les vraies valeurs des variables sensibles (à ne pas divulguer)
    et à renommer en secrets.py
"""

#: La clé secrète de Django, utilisée comme partie secrète des hash.
#: 
#: À remplacer par une valeur unique et à ne pas divulguer.
SECRET_KEY = 'INSERT YOUR DJANGO SECRET KEY HERE'
