Génération des pages web
========================

Les modules suivants sont mis à contributions pour répondre
aux requêtes web de l'utilisateur.

.. toctree::
   :maxdepth: 2

   urls
   views
   utilities
   ajaj
