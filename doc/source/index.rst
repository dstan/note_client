.. Client Django pour la NoteKfet2015 documentation master file, created by
   sphinx-quickstart on Mon Aug 26 12:53:12 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation du Client Django pour la NoteKfet2015
====================================================================

Table des matières :

.. toctree::
   :maxdepth: 3

   note/index

   keep_alive
   secrets
   settings
   urls

   others

Index & co
==========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

